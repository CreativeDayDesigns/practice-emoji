//
//  ViewController.swift
//  EmojiDescriber
//
//  Created by Christopher Wulle on 7/29/17.
//  Copyright © 2017 Christopher Wulle. All rights reserved.
//
//  Added some git commits to this project under Swift/EmojiDescriber

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    
    var emojis : [Emoji] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        emojis = makeEmojiArray()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return emojis.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let emoji = emojis[indexPath.row]
        cell.textLabel?.text = emoji.stringEmoji
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let emoji = emojis[indexPath.row]
        performSegue(withIdentifier: "moveSegue", sender: emoji)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let defVC = segue.destination as! DefinitionViewController
        defVC.emoji = sender as! Emoji
    }
    
    func makeEmojiArray() -> [Emoji] {
        let emoji1 = Emoji()
        emoji1.stringEmoji = "☃️"
        emoji1.category = "Thing"
        emoji1.birtyYear = 2010
        emoji1.definition = "A snowman with snowfall"
        
        let emoji2 = Emoji()
        emoji2.stringEmoji = "💩"
        emoji2.category = "Thing"
        emoji2.birtyYear = 2008
        emoji2.definition = "A piece of poo!"
        
        let emoji3 = Emoji()
        emoji3.stringEmoji = "😎"
        emoji3.category = "Smiley"
        emoji3.birtyYear = 2005
        emoji3.definition = "A cool dude smiley face"
        
        let emoji4 = Emoji()
        emoji4.stringEmoji = "🍦"
        emoji4.category = "Food"
        emoji4.birtyYear = 2010
        emoji4.definition = "An ice cream cone"
        
        let emoji5 = Emoji()
        emoji5.stringEmoji = "🏌🏻"
        emoji5.category = "Person"
        emoji5.birtyYear = 2012
        emoji5.definition = "A male golfer, blue shirt"
        
        let emoji6 = Emoji()
        emoji6.stringEmoji = "🎯"
        emoji6.category = "Person"
        emoji6.birtyYear = 2012
        emoji6.definition = "Bullseye!!!!"
        
        return [emoji1, emoji2, emoji3, emoji4, emoji5, emoji6]
    }


}

